﻿using System;
using System.Globalization;

/// <summary>
/// Stores the C# codebehind data for the Results.aspx page. Checks if a session state object called Votes exists, 
/// and if so loads the stored votes into the page.
/// </summary>
public partial class Results : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Votes"] == null)
        {
            return;
        }
        var votes = (int[])Session["Votes"];
        this.lblPTotal.Text = votes[0].ToString(CultureInfo.InvariantCulture);
        this.lblQTotal.Text = votes[1].ToString(CultureInfo.InvariantCulture);
        this.lblVoteTotal.Text = votes[2].ToString(CultureInfo.InvariantCulture);
    }
}