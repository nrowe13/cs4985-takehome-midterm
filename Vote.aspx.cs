﻿using System;
using System.Collections.Generic;

/// <summary>
/// Stores the C# codebehind data for the Vote.aspx page. Allows for the checking of/creation of a session state object called 
/// Votes and adds the user's vote into it so it may be loaded into the Results.aspx page.
/// </summary>
public partial class Vote : System.Web.UI.Page
{

    protected void btnVote_Click(object sender, EventArgs e)
    {
        int[] votes;
        if (Session["Votes"] != null)
        {
            votes = (int[]) Session["Votes"];
        }
        else
        {
            votes = new [] {0, 0, 0};
        }
        this.AddVotes(votes);
        Response.Redirect("Results.aspx");
    }

    private void AddVotes(IList<int> votes)
    {
        if (this.rbtnP.Checked)
        {
            votes[0]++;
        }
        else if (this.rbtnQ.Checked)
        {
            votes[1]++;
        }
        else
        {
            votes[2]++;
        }
        Session["Votes"] = votes;
    }
}