﻿using System;

/// <summary>
/// Stores the C# codebehind data for the Default.aspx page. Serves as the default greeting page for the website.
/// </summary>
public partial class Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}