﻿<%@ Page Language="C#" MasterPageFile="Vote.master" AutoEventWireup="true" CodeFile="Vote.aspx.cs" Inherits="Vote" %>

<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
     <link href="Styles/Vote.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="contentPlaceHolder" runat="server">
    <asp:Label ID="lblPoll" runat="server" Text="Poll for Ages 18-25" CssClass="voteHeadingLabel"/>
    <br/>
    <asp:Label ID="lblAge" runat="server" Text="Age:" CssClass="voteLabel"/>
    <asp:TextBox ID="tbxAge" runat="server" CssClass="voteText"/><asp:RangeValidator ID="rvAge" runat="server" ErrorMessage="Age must be between 18 and 25" ControlToValidate="tbxAge" Display="Dynamic" Type="Integer" MaximumValue="25" MinimumValue="18">Age must be between 18 and 25</asp:RangeValidator>
    <asp:RequiredFieldValidator ID="rfvAge" runat="server" ControlToValidate="tbxAge" Display="Dynamic">You must enter your age</asp:RequiredFieldValidator>
    <br/>
    <asp:Label ID="lblChoice" runat="server" Text="Your Choice:" CssClass="voteLabel"/>
    <asp:RadioButton ID="rbtnP" runat="server" GroupName="vote" Text="P" CssClass="voteRadio" /><asp:RadioButton ID="rbtnQ" runat="server" GroupName="vote" Text="Q" CssClass="voteRadio" />
    <br/>
    <br/>
    <asp:Button ID="btnVote" runat="server" Text="Cast Vote" OnClick="btnVote_Click" CssClass="voteButton" />
    <br/>
    <br/>
</asp:Content>
