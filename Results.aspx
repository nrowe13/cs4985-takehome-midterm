﻿<%@ Page Language="C#" MasterPageFile="Vote.master" AutoEventWireup="true" CodeFile="Results.aspx.cs" Inherits="Results" %>

<asp:Content ID="head" ContentPlaceHolderID="headPlaceHolder" runat="server">
     <link href="Styles/Results.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="contentPlaceHolder" runat="server">
    <asp:Label ID="lblResults" runat="server" Text="Voting Results" CssClass="resultsHeadingLabel"/>
    <br/>
    <asp:Label ID="lblPVotes" runat="server" Text="Votes for P: " CssClass="descriptionLabel"/>
    <asp:Label ID="lblPTotal" runat="server" Text="0" CssClass="countsLabel"/>
    <br/>
    <asp:Label ID="lblQVotes" runat="server" Text="Votes for Q: " CssClass="descriptionLabel"/>
    <asp:Label ID="lblQTotal" runat="server" Text="0" CssClass="countsLabel"/>
    <br/>
    <asp:Label ID="lblTotalVotes" runat="server" Text="Total Votes: " CssClass="descriptionLabel"/>
    <asp:Label ID="lblVoteTotal" runat="server" Text="0" CssClass="countsLabel"/>
    <br/>
    <br/>
</asp:Content>

